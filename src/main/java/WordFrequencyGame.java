import java.util.*;

public class WordFrequencyGame {
    public String getResult(String inputStr) {
        String splitRegex = "\\s+";
        if (inputStr.split(splitRegex).length == 1) {
            return inputStr + " 1";
        }
        try {
            String[] inputWords = inputStr.split(splitRegex);
            List<Input> inputFormList = new ArrayList<>();
            for (String word : inputWords) {
                Input singleInput = new Input(word, 1);
                inputFormList.add(singleInput);
            }
            inputFormList = generateInputFormList(inputFormList);
            return generatePrintMessage(inputFormList);
        } catch (Exception e) {
            return "Calculate Error";
        }
    }

    private String generatePrintMessage(List<Input> inputFormList) {
        StringJoiner joiner = new StringJoiner("\n");
        for (Input input : inputFormList) {
            String value = input.getValue() + " " + input.getWordCount();
            joiner.add(value);
        }
        return joiner.toString();
    }

    private List<Input> generateInputFormList(List<Input> inputFormList) {
        Map<String, List<Input>> wordCountMap = getListMap(inputFormList);

        List<Input> countInputList = new ArrayList<>();
        for (Map.Entry<String, List<Input>> entry : wordCountMap.entrySet()) {
            Input resultInput = new Input(entry.getKey(), entry.getValue().size());
            countInputList.add(resultInput);
        }
        inputFormList = countInputList;

        inputFormList.sort((w1, w2) -> w2.getWordCount() - w1.getWordCount());
        return inputFormList;
    }


    private Map<String, List<Input>> getListMap(List<Input> inputFormList) {
        Map<String, List<Input>> wordCountMap = new HashMap<>();
        for (Input singleInput : inputFormList) {
            if (!wordCountMap.containsKey(singleInput.getValue())) {
                ArrayList inputList = new ArrayList<>();
                inputList.add(singleInput);
                wordCountMap.put(singleInput.getValue(), inputList);
            } else {
                wordCountMap.get(singleInput.getValue()).add(singleInput);
            }
        }
        return wordCountMap;
    }
}
